$(function() {
	var h = $(window).height();
	$('#loaderBg ,#loader').height(h).css('display','block');

	$(window).load(function () { 
		$('#loaderBg').animate({
			opacity: '0'
		},'800').queue(function() {
			$('#loaderBg').remove();
		});
		$('#wrap').css('opacity', '1');
	});

	// anchor link クリック時のfunction
	$('a[href^=#]').click(function(){ 
		var speed = 1000; 
		var href= $(this).attr("href"); 
		var target = $(href == "#" || href == "" ? 'html' : href); 
		var position = target.offset().top; 
		$("html, body").animate({scrollTop:position}, speed, "swing"); 
		return false; 
	}); 

	// page loading時にqueryを読んでsmooth scroll
	$(window).load(function(){
		var url = $(location).attr('href');
		var ost = 0;
		if (url.indexOf("?id=") == -1) {
		} else {
			// スムーズスクロールの処理
			var url_sp = url.split("?id=");
			var hash = '#' + url_sp[url_sp.length - 1];
			var tgt = $(hash);
			var pos = tgt.offset().top - ost;
			$("html, body").animate({scrollTop:pos}, 1000, "swing");
		}
	});

	var topBtn = $('#pageTop');
	topBtn.hide();
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});

	// scroll時のアニメーション対応
	$(".scrEvent").each(function(){
		var imgPos = $(this).offset().top;
		var scroll = $(window).scrollTop();
		var windowHeight = $(window).height();
		if (scroll > imgPos - windowHeight + windowHeight/4){
			$(this).addClass('eventOn');
		}
	});
	$(window).scroll(function (){
		$(".scrEvent").each(function(){
			var imgPos = $(this).offset().top;
			var scroll = $(window).scrollTop();
			var windowHeight = $(window).height();
			if (scroll > imgPos - windowHeight + windowHeight/4){
				$(this).addClass('eventOn');
			}
		});
	});
});