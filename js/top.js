$(function() {

	var slider = $('.slider').bxSlider({
		mode: 'fade',
		auto: true,
		autoDelay: 4000,
		pause: 5000,
		onSliderLoad: function(i){
			fixMainvisualHeight(i);
			$('.slider .slide:nth-child(1)').addClass('on');
		},
		onSlideAfter: function(e, oi, ni){
			changeSlideIn(ni, oi);
			if(ni == 3) {
				stop_slider();
				setTimeout(function() {
					start_slider();
				}, 4000);
			}
		}
	});

	/**
	 * resize function
	 * # sp measures.
	 */
	function fixMainvisualHeight(i) {
		var wh = $(window).height();
		var hh = $('#header').outerHeight();
		var nh = $('#gnav').outerHeight();
		var th = hh + nh;
		var h = wh - th;
		var minH = 400;
		var maxH = 800;
		
		if(h < minH) {
			h = minH;
		}
		if(h > maxH) {
			h = maxH;
		}
		$('.bx-viewport').height(h);
		$('.slider').height(h);
		$('.slider .slide').height(h);
	}

	var windowWidth = $(window).width();
	$(window).resize(function(){
		var ww = $(window).width();
		if(windowWidth != ww) {
			fixMainvisualHeight();
			windowWidth = ww;
		}
	});

	function changeSlideIn(ni, oi) {
		console.log(ni);
		if(ni == 0) {
			$('.slider .slide').eq(0).addClass('on');
			$('.slider .slide').eq(1).removeClass('on');
			$('.slider .slide').eq(2).removeClass('on');
		}else if(ni == 1) {
			$('.slider .slide').eq(0).removeClass('on');
			$('.slider .slide').eq(1).addClass('on');
			$('.slider .slide').eq(2).removeClass('on');
		}else if(ni == 2) {
			$('.slider .slide').eq(0).removeClass('on');
			$('.slider .slide').eq(1).removeClass('on');
			$('.slider .slide').eq(2).addClass('on');
		}
	}

	function stop_slider() {
		slider.stopAuto();
	}

	function start_slider() {
		slider.startAuto();
	}
});