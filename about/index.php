<!DOCTYPE HTML>
<html lang="ja">
<head>
<?php require_once(dirname(__FILE__).'/../app_config.php'); ?>
<?php include(APP_PATH.'/libs/meta.php'); ?>
<link rel="stylesheet" href="<?php echo APP_URL;?>css/about.css">
</head>





<body>
<div id="wrap">
<!--
==================================================
header
==================================================
-->
<?php include(APP_PATH.'/libs/header.php'); ?>





<!--
==================================================
main contents
==================================================
-->
<main id="main">
	
	<section id="mainVisual">
		<h1><span class="inner"><b>A</b>BOUT US</span></h1>
		<div class="imageBox">
			<h2>Top message</h2>
			<img src="<?php echo APP_URL;?>images/about/mainimg.png" alt="">
			<p><img src="<?php echo APP_URL;?>images/about/mainimg_txt.png" alt="" width="438"></p>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	READ
	==================================================
	-->
	<section id="read" class="section scrEvent">
		<div class="inner">
			<h1>Company’s profile</h1>
			<p>アドナックは1987年の設立以来、「流行をとらえると言うよりは、市場を肌で感じる！」をスローガンに変わることのない本質的なモノを大切にする企業として社会に貢献します。</p>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	私たちについて
	==================================================
	-->
	<section id="about" class="section scrEvent">
		<div class="inner">
			<h1>私たちについて</h1>
			<p>
				カタチのない
				<span><img src="<?php echo APP_URL;?>images/about/about_txt_moya.png" alt="モヤモヤ" width="104"></span>
				をしっかりと
				<span><img src="<?php echo APP_URL;?>images/about/about_txt_katachi.png" alt="カタチ" width="68"></span>
				にしていくために
			</p>
			<ul>
				<li>
					<span>
					今度の新商品、<br>
					どんなデザイン、<br>
					パッケージに<br>
					しようかな…</span>
				</li>
				<li>
					<span>今ある商品を<br>
					さらに売り出したい。<br>
					どうやって<br>
					プロモーション展開<br>
					しようかな…</span>
				</li>
				<li>
					<span>
					発売日が<br>
					迫っているのに<br>
					間に合うかな…</span>
				</li>
				<li>
					<span>あれは<br>
					いくつあったっけ？<br>
					在庫の管理って<br>
					大変…</span>
				</li>
				<li>
					<span>
					輸入商品の検品、<br>
					仕上げ加工、<br>
					一括して<br>
					できないかな…
					</span>
				</li>
			</ul>
			<p>モノ･サービスを売りたい。その想いを実現するには様々な工程を要します。<br>
				アドナックはデザイン・パッケージ・プロモーション・アッセンブリの幅広い経験をもとに、<br>
				お客様に合わせた提案と満足度の高いサービスが提供できるよう全力で取り組んでいます。</p>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	会社概要
	==================================================
	-->
	<section id="company" class="section scrEvent">
		<div class="inner">
			<h1>会社概要</h1>
			<dl>
				<dt>社名</dt>
				<dd>株式会社 アドナック</dd>
			</dl>
			<dl>
				<dt>英文名</dt>
				<dd>ADNAC Co.,ltd</dd>
			</dl>
			<dl>
				<dt>代表者</dt>
				<dd>代表取締役社長　髙橋 大介</dd>
			</dl>
			<dl>
				<dt>設立</dt>
				<dd>1987年8月20日</dd>
			</dl>
			<dl>
				<dt>所在地</dt>
				<dd>〒110-0016 東京都台東区台東3-15-1 京阪御徒町ビル 2F<br>
					<a href="<?php echo APP_URL;?>?id=access">MAPはこちら</a><br>
					TEL：03-5846-2011（代表）<br>
					FAX：03-5846-2012
				</dd>
			</dl>
			<dl>
				<dt>URL</dt>
				<dd>http://www.adnac.co.jp</dd>
			</dl>
			<dl>
				<dt>資本金</dt>
				<dd>1,000万円</dd>
			</dl>
			<dl>
				<dt>取引金融機関</dt>
				<dd>三菱東京UFJ銀行 秋葉原駅前支店<br>みずほ銀行 上野支店</dd>
			</dl>
			<dl>
				<dt>関連会社</dt>
				<dd>株式会社 ビッグハンド（埼玉県八潮市）<br>URL ：http://www.bighand.co.jp</dd>
			</dl>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	主な取引先
	==================================================
	-->
	<section id="client" class="section scrEvent">
		<div class="inner">
			<h1>主な取引先<span>(敬称略・50音順)</span></h1>
			
			<ul>
				<li>株式会社石澤研究所<li>
				<li>株式会社井田ラボラトリーズ<li>
				<li>株式会社井田両国堂<li>
				<li>msh株式会社<li>
				<li>株式会社オーピーラボ<li>
				<li>株式会社シースタイル<li>
				<li>株式会社シャンティ<li>
				<li>株式会社ジャンパール<li>
				<li>株式会社スタイリングライフ・ホールディングス<li>
				<li>　　　　プラザスタイル カンパニー<li>
				<li>株式会社セザンヌ化粧品<li>
				<li>株式会社ハウスオブローゼ<li>
				<li>株式会社ヤマサキ</li>
			</ul>
			
			<ul>
				<li><b>OEM</b></li>
				<li>アサヌマコーポレーション株式会社</li>
				<li>株式会社アリエ</li>
				<li>株式会社アンズコーポレーション</li>
				<li>株式会社クラブコスメチックス</li>
				<li>ジェイオーコスメティックス株式会社</li>
				<li>株式会社セレス</li>
				<li>東色ピグメント株式会社</li>
				<li>東洋ビューティ株式会社</li>
				<li>株式会社東洋ビューティサプライ</li>
				<li>株式会社トキワ</li>
				<li>株式会社ナリス化粧品</li>
				<li>日本コルマー株式会社</li>
				<li>株式会社日本色材工業研究所</li>
				<li>株式会社ボナンザ</li>
				<li>株式会社宮内</li>
				<li>株式会社ミロット</li>
			</ul>
		</div>
	</section>
	
	
	
	
	
	
</main>





<!--
==================================================
footer
==================================================
-->
<?php include(APP_PATH.'/libs/footer.php'); ?>
</div>

<?php include(APP_PATH.'/libs/loader.php'); ?>
</body>
</html>
