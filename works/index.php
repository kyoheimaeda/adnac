<!DOCTYPE HTML>
<html lang="ja">
<head>
<?php require_once(dirname(__FILE__).'/../app_config.php'); ?>
<?php include(APP_PATH.'/libs/meta.php'); ?>
<link rel="stylesheet" href="<?php echo APP_URL;?>css/works.css">
</head>





<body>
<div id="wrap">
<!--
==================================================
header
==================================================
-->
<?php include(APP_PATH.'/libs/header.php'); ?>





<!--
==================================================
main contents
==================================================
-->
<main id="main">
	<div id="read">
		<h1><span class="inner"><b>W</b>ORKS</span></h1>
	</div>
	
	
	
	<!--
	==================================================
	Package＆Sales promotion
	==================================================
	-->
	<section id="works01" class="section">
		<div class="inner">
			<h1 class="scrEvent">Package＆Sales promotion</h1>
			<div class="itemList">
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img01@1x.jpg" width="395" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img02@1x.jpg" width="638" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img03@1x.jpg" width="512" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img04@1x.jpg" width="496" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img05@1x.jpg" width="486" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12">
						<div>
							<figure><img src="<?php echo APP_URL;?>images/works/works01_img06@1x.jpg" width="465" alt=""></figure>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	Package
	==================================================
	-->
	<section id="works02" class="section">
		<div class="inner">
			<h1>Package</h1>
			<div class="itemList">
				<ul class="row">
					<li class="scrEvent col col-8">
						<div class="heightLine-img01">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img01@1x.jpg" width="365" alt="196"></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img01">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img02@1x.jpg" width="159" height="164" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img02">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img03@1x.jpg" width="59" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img02">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img04@1x.jpg" width="151" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img02">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img05@1x.jpg" width="132" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img03">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img06@1x.jpg" width="209" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-8">
						<div class="heightLine-img03">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img07@1x.jpg" width="384" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img04">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img08@1x.jpg" width="171" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img04">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img09@1x.jpg" width="202" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img04">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img10@1x.jpg" width="68" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img05">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img11@1x.jpg" width="96" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img05">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img12@1x.jpg" width="64" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img05">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img13@1x.jpg" width="64" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img06">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img14@1x.jpg" width="139" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img06">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img15@1x.jpg" width="115" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img06">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img16@1x.jpg" width="129" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img07">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img17@1x.jpg" width="121" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img07">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img18@1x.jpg" width="83" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img07">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img19@1x.jpg" width="105" alt=""></figure>
						</div>
					</li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-4">
						<div class="heightLine-img08">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img20@1x.jpg" width="137" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img08">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img21@1x.jpg" width="119" alt=""></figure>
						</div>
					</li>
					<li class="scrEvent col col-4">
						<div class="heightLine-img08">
							<figure><img src="<?php echo APP_URL;?>images/works/works02_img22@1x.jpg" width="123" alt=""></figure>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	Package
	==================================================
	-->
	<section id="works03" class="section">
		<div class="inner">
			<h1>Sales promotion</h1>
			<div class="itemList">
				<ul class="row">
					<li class="scrEvent col col-12"><img src="<?php echo APP_URL;?>images/works/works03_img01@1x.jpg" width="687" height="221" alt=""></li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12"><img src="<?php echo APP_URL;?>images/works/works03_img02@1x.jpg" width="562" alt=""></li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12"><img src="<?php echo APP_URL;?>images/works/works03_img03@1x.jpg" width="850" alt=""></li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-6"><img src="<?php echo APP_URL;?>images/works/works03_img04@1x.jpg" width="215" alt=""></li>
					<li class="scrEvent col col-6"><img src="<?php echo APP_URL;?>images/works/works03_img05@1x.jpg" width="228" alt=""></li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-6"><img src="<?php echo APP_URL;?>images/works/works03_img06@1x.jpg" width="200" alt=""></li>
					<li class="scrEvent col col-6"><img src="<?php echo APP_URL;?>images/works/works03_img07@1x.jpg" width="183" alt=""></li>
				</ul>
				<ul class="row">
					<li class="scrEvent col col-12"><img src="<?php echo APP_URL;?>images/works/works03_img08@1x.jpg" width="620" alt=""></li>
				</ul>
			</div>
		</div>
	</section>
	
	
	
</main>





<!--
==================================================
footer
==================================================
-->
<?php include(APP_PATH.'/libs/footer.php'); ?>
</div>

<?php include(APP_PATH.'/libs/loader.php'); ?>

</body>
</html>
