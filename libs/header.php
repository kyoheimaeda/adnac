<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-81145923-1', 'auto');
  ga('send', 'pageview');

</script>

<header id="header">
	<div class="inner">
		<h1><a href="<?php echo APP_URL;?>"><img src="<?php echo APP_URL;?>images/common/h_logo.png" alt="ADNAC" width="212"></a></h1>
		<ul>
			<li>
				<a href="mailto:info@adnac.co.jp">
					<span class="ico"><img src="<?php echo APP_URL;?>images/common/h_ico_mail.png" alt=""></span>
					<span class="txt">お問い合わせはこちら</span>
				</a>
			</li>
			<li>
				<a href="http://bighand.co.jp" target="_blank">
					<span class="ico"><img src="<?php echo APP_URL;?>images/common/h_ico_hand.png" alt=""></span>
					<span><img src="<?php echo APP_URL;?>images/common/h_logo_hand.png" alt="Bighand　アッセンブリならビッグハンド"></span>
					<span class="txt">アッセンブリなら<br>ビッグハンド</span>
				</a>
			</li>
		</ul>
	</div>
</header>



<nav id="gnav">
	<ul class="inner">
		<li><a href="<?php echo APP_URL;?>about">ABOUT US</a></li>
		<li><a href="<?php echo APP_URL;?>services">SERVICES</a></li>
		<li><a href="<?php echo APP_URL;?>works">WORKS</a></li>
		<li><a href="<?php echo APP_URL;?>recruit">RECRUIT</a></li>
	</ul>
</nav>