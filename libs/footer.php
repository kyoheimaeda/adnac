<p id="pageTop"><a href="#"></a></p>

<nav id="footerNav">
	<div class="inner">
		<ul>
			<li><a href="<?php echo APP_URL;?>about">ABOUT US</a></li>
			<li><a href="<?php echo APP_URL;?>services">SERVICES</a></li>
			<li><a href="<?php echo APP_URL;?>works">WORKS</a></li>
			<li><a href="<?php echo APP_URL;?>recruit">RECRUIT</a></li>
		</ul>
	</div>
</nav>


<footer id="footer">
	<div class="inner">
		<h1><img src="<?php echo APP_URL;?>images/common/h_logo.png" alt="ADNAC" width="212"></h1>
		<address>〒110-0016 東京都台東区台東3-15-1 京阪御徒町ビル2F</address>
	</div>
	<small>&copy 2016 ADNAC Co.,ltd. All Rights Reserved.</small>
</footer>


<!--
========================================
Java Script
========================================
-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://api.html5media.info/1.1.6/html5media.min.js"></script>

<!--[if lt IE 9]>
<script src="<?php echo APP_URL;?>js/html5shiv.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo APP_URL;?>js/flexibility.js"></script>
<link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Meie+Script' rel='stylesheet' type='text/css'>
<![endif]-->

<script src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="<?php echo APP_URL;?>js/heightLine.js"></script>
<script type="text/javascript" src="<?php echo APP_URL;?>js/common.js"></script>
