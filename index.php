<!DOCTYPE HTML>
<html lang="ja">
<head>
<?php require_once(dirname(__FILE__).'/app_config.php'); ?>
<?php include(APP_PATH.'/libs/meta.php'); ?>
<link rel="stylesheet" href="<?php echo APP_URL;?>js/jquery.bxslider/jquery.bxslider.css">
<link rel="stylesheet" href="<?php echo APP_URL;?>css/top.css">
</head>





<body>
<div id="wrap">
<!--
==================================================
header
==================================================
-->
<?php include(APP_PATH.'/libs/header.php'); ?>





<!--
==================================================
main contents
==================================================
-->
<main id="main">
	
	
	<!--
	==================================================
	MAIN VISUAL
	==================================================
	-->
	<div id="main-visual">
		<ul class="slider">
			<li class="slide">
				<div class="line"></div>
				<div class="slide-inner">
					<p class="txt"><img src="<?php echo APP_URL;?>images/top/mainimg01_txt.png" alt="ADNAC　つくる（創・作・造）を楽しむ" width="622" height="323"></p>
					<p class="balloon"><img src="<?php echo APP_URL;?>images/top/mainimg01_balloon.png" alt="おかげさまで30週年" width="195" height="188"></p>
				</div>
			</li>
			<li class="slide">
				<div class="line"></div>
				<div class="slide-inner">
					<p class="txt"><img src="<?php echo APP_URL;?>images/top/mainimg02_txt.png" alt="" width="413" height="50"></p>
				</div>
			</li>
			<li class="slide">
				<div class="line"></div>
				<div class="slide-inner">
					<p class="txt"><img src="<?php echo APP_URL;?>images/top/mainimg03_txt.png" alt="" width="306" height="49"></p>
				</div>
			</li>
		</ul>
	</div>
	
	
	<section id="news" class="scrEvent">
		<div class="inner">
			<h1>NEWS</h1>
			<dl>
				<dt>2016.8.1</dt>
				<dd>ホームページをリニューアルいたしました。<br>今後ともアドナックをよろしくお願いいたします。</dd>
			</dl>
		</div>
	</section>
	
	
	<!--
	==================================================
	ABOUT US
	==================================================
	-->
	<section id="about" class="section scrEvent">
		<div class="inner">
			<h1>
				<span><b>A</b>BOUT US</span>
				<span>企業情報</span>
			</h1>
			
			<ul class="row">
				<li class="col col-6">
					<a href="<?php echo APP_URL;?>about">
						<figure><img src="<?php echo APP_URL;?>images/top/about_img01.png" alt="" width="95" height="97"></figure>
						<h2>
							<span class="en">Top message</span>
							<span>私たちが伝えたいこと</span>
						</h2>
					</a>
				</li>
				<li class="col col-6">
					<a href="<?php echo APP_URL;?>about?id=company">
						<figure><img src="<?php echo APP_URL;?>images/top/about_img02.png" alt="" width="104" height="76"></figure>
						<h2>
							<span>会社概要</span>
							<span class="en">Company’s<br>profile</span>
							<span>私たちについて</span>
						</h2>
					</a>
				</li>
			</ul>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	SERVICES
	==================================================
	-->
	<section id="services" class="section scrEvent">
		<div class="inner">
			<h1>
				<span><b>S</b>ERVICES</span>
				<span>事業内容</span>
			</h1>
			
			<ul class="row">
				<li class="col col-4">
					<a href="<?php echo APP_URL;?>services?id=read">
						<figure><img src="<?php echo APP_URL;?>images/top/services_img01.png" alt="" width="82" height="72"></figure>
						<h2>
							<span>コンセプト</span>
							<span class="en">Concept</span>
							<span>私たちの仕事について</span>
						</h2>
					</a>
				</li>
				<li class="col col-4">
					<a href="<?php echo APP_URL;?>services?id=planning">
						<figure><img src="<?php echo APP_URL;?>images/top/services_img02.png" alt="" width="58" height="89"></figure>
						<h2>
							<span>プランニング&<br>デザイン</span>
							<span class="en">Planning<br>&<br>design</span>
							<span>心を動かす<br>アイデアとデザインを</span>
						</h2>
					</a>
				</li>
				<li class="col col-4">
					<a href="<?php echo APP_URL;?>services?id=package">
						<figure><img src="<?php echo APP_URL;?>images/top/services_img03.png" alt="" width="46" height="103"></figure>
						<h2>
							<span>パッケージ</span>
							<span class="en">Package</span>
							<span>商品の魅力を<br>最大限に引き出す</span>
						</h2>
					</a>
				</li>
			</ul>
			
			<ul class="row">
				<li class="col col-6">
					<a href="<?php echo APP_URL;?>services?id=sales">
						<figure><img src="<?php echo APP_URL;?>images/top/services_img04.png" alt="" width="88" height="77"></figure>
						<h2>
							<span>セールスプロモーション</span>
							<span class="en">Sales<br>Promotion</span>
							<span>価値ある店舗<br>コミュニケーションのご提案</span>
						</h2>
					</a>
				</li>
				<li class="col col-6">
					<a href="<?php echo APP_URL;?>services?id=assembly">
						<figure><img src="<?php echo APP_URL;?>images/top/services_img05.png" alt="92" width="79" height=""></figure>
						<h2>
							<span>アッセンブリ</span>
							<span class="en">Assembly</span>
							<span>アッセンブリで<br>お客様の課題を解決</span>
						</h2>
					</a>
				</li>
			</ul>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	WORKS
	==================================================
	-->
	<section id="works" class="section scrEvent">
		<div class="inner">
			<h1>
				<span><b>W</b>ORKS</span>
				<span>制作実績</span>
			</h1>
			
			<ul class="row">
				<li class="col col-12">
					<a href="<?php echo APP_URL;?>works">
						<figure><img src="<?php echo APP_URL;?>images/top/works_img01.png" alt="" width="372" height="114"></figure>
						<h2>
							<span class="en">Works</span>
							<span>制作実績</span>
						</h2>
					</a>
				</li>
			</ul>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	RECRUIT
	==================================================
	-->
	<section id="recruit" class="section scrEvent">
		<div class="inner">
			<h1>
				<span><b>R</b>ECRUIT</span>
				<span>採用案内</span>
			</h1>
			
			<ul class="row">
				<li class="col col-12">
					<a href="<?php echo APP_URL;?>recruit">
						<figure><img src="<?php echo APP_URL;?>images/top/recruit_img01.png" alt="" width="280" height="111"></figure>
						<h2>
							<span class="en">Recruit</span>
							<span>採用案内</span>
						</h2>
					</a>
				</li>
			</ul>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	ACCESS
	==================================================
	-->
	<section id="access" class="section scrEvent">
		<div class="inner">
			<h1>
				<span><b>A</b>CCESS</span>
				<span>アクセス</span>
			</h1>
		</div>
		<div id="mapWrap">
			<div id="accessBox">
				<h2>ACCESS</h2>
				<dl>
					<dt>JR</dt>
					<dd>御徒町駅南口より  徒歩6分</dd>
					<dt>日比谷線</dt>
					<dd>仲御徒町駅1番出口より  徒歩3分</dd>
					<dt>銀座線</dt>
					<dd>末広町駅2番出口より  徒歩7分</dd>
					<dt>大江戸線</dt>
					<dd>上野御徒町駅A6番出口より  徒歩8分</dd>
					<dt>JR・つくばエクスプレス</dt>
					<dd>秋葉原駅 JR中央改札口<br>つくばエクスプレスA3出口より 徒歩10分</dd>
				</dl>
			</div>
			<div id="mapCanvas"></div>
		</div>
	</section>
	
</main>





<!--
==================================================
footer
==================================================
-->
<?php include(APP_PATH.'/libs/footer.php'); ?>
</div>

<?php include(APP_PATH.'/libs/loader.php'); ?>




<script type="text/javascript" src="<?php echo APP_URL;?>js/jquery.bxslider/jquery.bxslider.min.js"></script>


<script>
var map;
function initMap() {
	// マップのスタイル
	var style = [
		{
		"stylers": [
			{ "saturation": -100 }
		]
		}
	];
	// キャンパスの要素を取得する
	var map = document.getElementById( 'mapCanvas' ) ;
	var latlng = new google.maps.LatLng( 35.704342, 139.776333 );
	
	// 地図のオプションを設定する
	var mapOptions = {
		styles: style,
		zoom: 18 ,				// ズーム値
		center: latlng ,		// 中心座標 [latlng]
		scrollwheel: false,
	};
	
	// [canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
	var map = new google.maps.Map( map , mapOptions ) ;
	
	
	
	// マーカー表示
	var image = {
		url : 'images/common/map_maker.png',
		scaledSize : new google.maps.Size(46, 59)
	}
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(35.704342, 139.776333),
		map: map,
		icon : image
	});
	
	//情報ウィンドウ描画
	var popup = new google.maps.InfoWindow({
		content: "吹き出しのコメントをここに記述します"
	});
	
	//マーカーをクリックした場合、吹き出しを表示
	google.maps.event.addListener(marker,"click",function(){popup.open(map, marker);});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNwdxzD_Ziz63LDbVdpA91EUO_DqzflgQ&callback=initMap"
        async defer></script>
</body>
</html>
