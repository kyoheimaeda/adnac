<!DOCTYPE HTML>
<html lang="ja">
<head>
<?php require_once(dirname(__FILE__).'/../app_config.php'); ?>
<?php include(APP_PATH.'/libs/meta.php'); ?>
<link rel="stylesheet" href="<?php echo APP_URL;?>css/services.css">
</head>





<body>
<div id="wrap">
<!--
==================================================
header
==================================================
-->
<?php include(APP_PATH.'/libs/header.php'); ?>





<!--
==================================================
main contents
==================================================
-->
<main id="main">
	
	<section id="mainVisual">
		<h1><span class="inner"><b>S</b>ERVICES</span></h1>
		<div class="imageBox">
			<img src="<?php echo APP_URL;?>images/services/mainimg@1x.jpg" alt="" width="1200" height="410">
		</div>
	</section>
	
	
	
	<!--
	==================================================
	READ
	==================================================
	-->
	<section id="read" class="section scrEvent">
		<div class="inner">
			<h1>「どうしよう？」を「こうしよう！」に。</h1>
			<p>アドナックは商品の企画デザインからパッケージの制作、プロモーションの企画、商品の仕上げ作業等、<br>お客様の「困った」を解決する為、全ての工程でのお客様のニーズを考え提案いたします。</p>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	ADNAC
	==================================================
	-->
	<section id="proposal" class="section">
		<div id="adnac" class="scrEvent">
			<div class="inner">
				<h1><img src="<?php echo APP_URL;?>images/services/adnac_logo.png" alt="ADNAC" width="208"></h1>
				<ul>
					<li>
						<b>Planning & <br>design</b>
						プランニング&<br>デザイン
					</li>
					<li>
						<b>Package</b>
						パッケージ
					</li>
					<li>
						<b>Sales<br>promotion</b>
						セールス<br>プロモーション
					</li>
					<li>
						<b>Assembly</b>
						アッセンブリ
					</li>
				</ul>
				<ul>
					<li>
						商品企画<br>
						ロゴデザイン<br>
						容器デザイン<br>
						パッケージデザイン<br>
						販促デザイン<br>
						VMDデザイン
					</li>
					<li>
						ラベル<br>
						化粧箱<br>
						クリアケース<br>
						ブリスター<br>
						シュリンクフィルム<br>
						袋
					</li>
					<li>
						カウンター什器<br>
						フロア什器<br>
						ハンガー什器<br>
						パネル<br>
						POP
					</li>
					<li>
						商品検品<br>
						商品仕上げ<br>
						販促セット<br>
						在庫管理<br>
						配送<br>
						<span>グループ会社 ビッグハンド</span>
					</li>
				</ul>
			</div>
		</div>
		<figure id="client" class="scrEvent">
			<figcaption>お客様</figcaption>
			<img src="<?php echo APP_URL;?>images/services/proposal_ico_humans.png" alt="" width="498">
		</figure>
	</section>
	
	
	
	<!--
	==================================================
	プランニング＆デザイン Planning & design
	==================================================
	-->
	<section id="planning" class="section02">
		<div class="inner">
			<h1 class="scrEvent">
				<i><img src="<?php echo APP_URL;?>images/services/planning_ico.png" alt="" width="50"></i>
				<span>
					プランニング&デザイン
					<em>Planning&design</em>
				</span>
			</h1>
			
			
			<div class="readBox scrEvent">
				<h2>0から1を生み出すために</h2>
				<p>私たちはお客様とのコミュニケーションの中に隠れた伝えるべき「本質」を見出すことからデザインを始めます。</p>
			</div>
			
			<div class="stepBox scrEvent">
				<h3>Step.01 コンセプトづくり</h3>
				<p>ヒット商品を生み出すためには、まず消費者に何を伝えるべきかを考えます。売り場のリサーチを繰り返し、参考デザインを集めた上でデザインコンセプトやキャッチコピー等を考え作成いたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/planning_img01@1x.jpg" alt=""></figure>
			</div>
			
			
			<div class="stepBox scrEvent">
				<h3>Step.02 ロゴから容器、パッケージのデザイン</h3>
				<p>1つの商品に対して商品ロゴやパッケージの形状までを含め、さまざまなバリエーションのデザインをコストを考えた上で提案いたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/planning_img02@1x.jpg" alt=""></figure>
			</div>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	パッケージ Package
	==================================================
	-->
	<section id="package" class="section02">
		<div class="inner">
			<h1 class="scrEvent">
				<i><img src="<?php echo APP_URL;?>images/services/package_ico.png" alt="" width="35"></i>
				<span>
					パッケージ
					<em>Package</em>
				</span>
			</h1>
			
			
			<div class="readBox scrEvent">
				<h2>商品価値を高めるために</h2>
				<p>消費者にとってワクワクする商品であるためにこだわりをもって作成いたします。</p>
			</div>
			
			
			<div class="stepBox scrEvent">
				<h3>Step.01 パッケージの形状と材質の決定</h3>
				<p>デザイン性だけではなくパッケージの材質、強度、印刷方法、コストまで考えた設計提案をいたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/package_img01@1x.jpg" alt="" width="850" height="354"></figure>
			</div>
			
			
			<div class="stepBox scrEvent">
				<h3>Step.02 パッケージの印刷と加工</h3>
				<p>ラベル、化粧箱（紙）、クリアケース、シュリンクフィルム等、パッケージの仕様によりさまざまな印刷方法を使い分け、現場での立会いを行い色や加工の再現性を確認いたします。印刷不良が混在しないよう画像検査等の確認作業を行い、納品まで責任を持って進行いたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/package_img02@1x.jpg" alt="" width="850" height="291"></figure>
			</div>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	セールスプロモーション Sales promotion
	==================================================
	-->
	<section id="sales" class="section02">
		<div class="inner">
			<h1 class="scrEvent">
				<i><img src="<?php echo APP_URL;?>images/services/sales_ico.png" alt="" width="110"></i>
				<span>
					セールスプロモーション
					<em>Sales promotion</em>
				</span>
			</h1>
			
			
			<div class="readBox scrEvent">
				<h2>思わず手にとりたくなる商品に</h2>
				<p>どうすれば売り場で商品を手に取ってもらえるか、価値あるプロモーション展開を提案いたします。</p>
			</div>
			
			
			<div class="stepBox scrEvent">
				<h3>Step.01 売り場での販促提案</h3>
				<p>パッケージだけではなく、什器やパネル、その他POP 等、店頭での販促展開も同時に考えます。商品の良さをわかりやすく目立つようにアピールするために、形状・キャッチコピー等から提案いたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/sales_img01@1x.jpg" alt=""></figure>
			</div>
			
			
			<div class="stepBox scrEvent">
				<h3>Step.02 展開に合わせたプロモーション</h3>
				<p>店頭に合わせ、さまざまな展開でのプロモーションを提案いたします。例えば１店舗だけの限定プロモーションなど手をかけた細かな販促も得意としています。見て楽しく、思わず買いたくなるような仕掛けを考え、制作いたします。</p>
				<figure><img src="<?php echo APP_URL;?>images/services/sales_img02@1x.jpg" alt=""></figure>
			</div>
		</div>
	</section>
	
	
	
	<!--
	==================================================
	アッセンブリ Assembly
	==================================================
	-->
	<section id="assembly" class="section02">
		<div class="inner">
			<h1 class="scrEvent">
				<i><img src="<?php echo APP_URL;?>images/services/assembly_ico.png" alt="" width="88"></i>
				<span>
					アッセンブリ
					<em>Assembly</em>
				</span>
			</h1>
			
			
			<div class="readBox scrEvent">
				<h2>商品や販促の仕上げまで</h2>
				<p>アドナックは関連会社ビッグハンドを立ち上げ、化粧品製造業の一般の薬事許可を取得しております。</p>
				<p>大切な商品や販促物が店頭に並ぶ最終仕上げ作業まで確実に管理いたします。<br>審査基準の高いクリーンルームを完備し、商品の仕上げ作業から検品や保管、販促物セット等、<br>きめ細かな対応をいたします。</p>
			</div>
			
			
			<div class="stepBox scrEvent">
				<figure><img src="<?php echo APP_URL;?>images/services/assembly_img01@1x.jpg" alt=""></figure>
				<p><a href="http://bighand.co.jp" target="_blank"><img src="<?php echo APP_URL;?>images/services/assembly_banner.png" alt="Bighand　アッセンブリならビッグハンド" width="240"></a></p>
			</div>
		</div>
	</section>
	
	
	
	
	
	
</main>





<!--
==================================================
footer
==================================================
-->
<?php include(APP_PATH.'/libs/footer.php'); ?>
</div>

<?php include(APP_PATH.'/libs/loader.php'); ?>
</body>
</html>
